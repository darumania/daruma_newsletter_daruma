from flask import Flask, render_template, request, redirect, url_for, make_response, jsonify, flash
from autosend import *
from api import *
import os
from markdown2 import Markdown
from _constants import *
from postgre import *
import threading

app = Flask(__name__)
app.secret_key = "random string"
# home page
@app.route('/')
def index():
    # result = getEntry()
    # return render_template('index.html', result = result)
    return redirect("/view_schedule")
@app.route('/preview/<string:id>')
def preview(id):

    if DEBUG_MODE != True:
        try:
            content = getContentById(id)

            entry_id = str(id)

            bannerurl = getAsset(id)

            # get collection key
            collectionkey = content.get('collectionKey', 'null')
            collectionkey = str(collectionkey).replace('.html', '') if collectionkey is not 'null' else 'null'
            collectionkey = str(collectionkey).replace('/catalog/', 'catalog/') if collectionkey is not 'null' else 'null'

            # get collection 2 key
            collection2key = content.get('collection2Key', 'null')
            collection2key = str(collection2key).replace('.html', '') if collection2key is not 'null' else 'null'
            collection2key = str(collection2key).replace('/catalog/','catalog/') if collection2key is not 'null' else 'null'

            # get leadParagraph
            leadParagraph = content.get('bodyText', None)
            leadParagraph = markdowner.convert(leadParagraph) if leadParagraph is not None else 'null'
            tag = leadParagraph[:3] if leadParagraph is not 'null' else 'null'
            if tag[1:] == 'h1' and tag is not 'null' or tag[1:] == 'h2' and tag is not 'null' or tag[
                                                                                                 1:] == 'h3' and tag is not 'null':
                tag += ' style="text-align:center;">'
                leadParagraph = leadParagraph[4:]
                leadParagraph = tag + leadParagraph
            # leadParagraph = 'hadeh'

            # subject
            subject = content.get('subject')

            # get keypath from detail page
            key_path = str(collectionkey).replace('/catalog/', '')
            key_path = str(key_path).replace('catalog/', '')
            key_path = key_path.replace('.html', '')

            # get keypath2 from detail page
            key_path2 = str(collection2key).replace('/catalog/', '')
            key_path2 = str(key_path2).replace('catalog/', '')
            key_path2 = key_path2.replace('.html', '')

            # get secondaryBanner1 url from detail page
            secondaryBanner1 = getSecondaryBanner1(id)

            # get secondaryBanner2 url from detail page
            secondaryBanner2 = getSecondaryBanner2(id)

            # get secondaryBanner3 url from detail page
            secondaryBanner3 = getSecondaryBanner3(id)

            # get secondaryBanner1Url from detail page
            secondaryBanner1Url = content.get('secondaryBanner1Url', None)

            # get secondaryBanner2Url from detail page
            secondaryBanner2Url = content.get('secondaryBanner2Url', None)

            # get secondaryBanner3Url from detail page
            secondaryBanner3Url = content.get('secondaryBanner3Url', None)

            # get collection product keys/code
            collectionProductKeys = content.get('collectionProductKeys', 'null')

            # get collection 2 product keys/code
            collection2ProductKeys = content.get('collection2ProductKeys', 'null')

            # get SecondaryBannerSectionTitle
            secondaryBannerSectionTitle = content.get('secondaryBannerSectionTitle')

            # get main banner url
            mainBannerUrl = content.get('mainBannerUrl')

            # get Asset ContentBlock
            keuntunganDaruma = getAssetContentBlock()
            keuntunganDaruma = 'http://' + str(keuntunganDaruma)

            # get footer banner from detail page
            footerBanner = getFooterBanner(id)

            # get Asset logoDaruma
            logoDaruma = getAssetContentBlockLogoDaruma()
            logoDaruma = 'http://' + str(logoDaruma)

            # get contentblock footer
            footer = getContentBlockFooter()

            # get footer banner url
            footerBannerUrl = content.get('footerBannerUrl', None)

            # get products

            if DEBUG_MODE == True:
                result = getProductByProductKeys(intoList(collectionProductKeys)) if collectionProductKeys is not 'null' else getProducts(key_path)
            else:
                try:
                    result = getProductByProductKeys(
                        intoList(collectionProductKeys)) if collectionProductKeys is not 'null' else getProducts(key_path)
                except Exception:
                    result = ''

            lists = getLists()

            result1 = result[:3]
            result2 = result[3:6]

            result_2 = getProductByProductKeys(
                intoList(collection2ProductKeys)) if collection2ProductKeys is not 'null' \
                else getProducts(key_path2)
            result1_2 = result_2[:3]
            result2_2 = result_2[3:6]

            return render_template('preview.html',
                                   entry_id=entry_id,
                                   bannerurl=bannerurl,
                                   key_path=key_path,
                                   result1=result1,
                                   result2=result2,
                                   result1_2=result1_2,
                                   result2_2=result2_2,
                                   collectionkey=collectionkey,
                                   collection2key=collection2key,
                                   leadParagraph=leadParagraph,
                                   subject=subject,
                                   secondaryBanner1=secondaryBanner1,
                                   secondaryBanner2=secondaryBanner2,
                                   secondaryBanner3=secondaryBanner3,
                                   secondaryBannerSectionTitle=secondaryBannerSectionTitle,
                                   keuntunganDaruma=keuntunganDaruma,
                                   logoDaruma=logoDaruma,
                                   footer=footer,
                                   secondaryBanner1Url=secondaryBanner1Url,
                                   secondaryBanner2Url=secondaryBanner2Url,
                                   secondaryBanner3Url=secondaryBanner3Url,
                                   mainBannerUrl=mainBannerUrl,
                                   footerBanner=footerBanner,
                                   result=result,
                                   collectionProductKeys=collectionProductKeys,
                                   collection2ProductKeys=collection2ProductKeys,
                                   lists=lists,
                                   footerBannerUrl=footerBannerUrl)
        except Exception as e:
            return jsonify({'error': str(e)})
    else:
        content = getContentById(id)

        bannerurl = getAsset(id)

        entry_id = str(id)

        # get collection key
        collectionkey = content.get('collectionKey', 'null')
        collectionkey = str(collectionkey).replace('.html', '') if collectionkey is not 'null' else 'null'
        collectionkey = str(collectionkey).replace('/catalog/','catalog/') if collectionkey is not 'null' else 'null'

        # get leadParagraph
        leadParagraph = content.get('bodyText', None)
        leadParagraph = markdowner.convert(leadParagraph) if leadParagraph is not None else 'null'
        tag = leadParagraph[:3] if leadParagraph is not 'null' else 'null'
        if tag[1:] == 'h1' and tag is not 'null' or tag[1:] == 'h2' and tag is not 'null' or tag[1:] == 'h3' and tag is not 'null':
            tag += ' style="text-align:center;">'
            leadParagraph = leadParagraph[4:]
            leadParagraph = tag + leadParagraph


        # subject
        subject = content.get('subject')

        # get keypath from detail page
        key_path = str(collectionkey).replace('/catalog/', '')
        key_path = str(key_path).replace('catalog/', '')
        key_path = key_path.replace('.html', '')

        # get collection 2 key
        collection2key = content.get('collection2Key', 'null')
        collection2key = str(collection2key).replace('.html', '') if collection2key is not 'null' else 'null'
        collection2key = str(collection2key).replace('/catalog/',
                                                     'catalog/') if collection2key is not 'null' else 'null'

        # get keypath2 from detail page
        key_path2 = str(collection2key).replace('/catalog/', '')
        key_path2 = str(key_path2).replace('catalog/', '')
        key_path2 = key_path2.replace('.html', '')


        # get secondaryBanner1 url from detail page
        secondaryBanner1 = getSecondaryBanner1(id)

        # get secondaryBanner2 url from detail page
        secondaryBanner2 = getSecondaryBanner2(id)

        # get secondaryBanner3 url from detail page
        secondaryBanner3 = getSecondaryBanner3(id)

        # get secondaryBanner1Url from detail page
        secondaryBanner1Url = content.get('secondaryBanner1Url', None)

        # get secondaryBanner2Url from detail page
        secondaryBanner2Url = content.get('secondaryBanner2Url', None)

        # get secondaryBanner3Url from detail page
        secondaryBanner3Url = content.get('secondaryBanner3Url', None)

        # get collection product keys/code
        collectionProductKeys = content.get('collectionProductKeys', 'null')

        # get collection 2 product keys/code
        collection2ProductKeys = content.get('collection2ProductKeys', 'null')

        # get SecondaryBannerSectionTitle
        secondaryBannerSectionTitle = content.get('secondaryBannerSectionTitle')

        # get main banner url
        mainBannerUrl = content.get('mainBannerUrl')

        # get Asset ContentBlock
        keuntunganDaruma = getAssetContentBlock()
        keuntunganDaruma = 'http://' + str(keuntunganDaruma)

        # get footer banner from detail page
        footerBanner = getFooterBanner(id)

        # get Asset logoDaruma
        logoDaruma = getAssetContentBlockLogoDaruma()
        logoDaruma = 'http://' + str(logoDaruma)

        # get contentblock footer
        footer = getContentBlockFooter()

        # get footer banner url
        footerBannerUrl = content.get('footerBannerUrl', None)

        # get products

        if DEBUG_MODE == True:
            result = getProductByProductKeys(intoList(collectionProductKeys)) if collectionProductKeys is not 'null' else getProducts(key_path)
        else:
            try:
                result = getProductByProductKeys(
                    intoList(collectionProductKeys)) if collectionProductKeys is not None else getProducts(key_path)
            except Exception:
                result = ''

        lists = getLists()

        result_2 = getProductByProductKeys(
            intoList(collection2ProductKeys)) if collection2ProductKeys is not 'null' \
            else getProducts(key_path2)
        result1_2 = result_2[:3]
        result2_2 = result_2[3:6]

        result1 = result[:3]
        result2 = result[3:6]

        return render_template('preview.html',
                               entry_id=entry_id,
                               bannerurl=bannerurl,
                               key_path=key_path,
                               result1=result1,
                               result2=result2,
                               result1_2=result1_2,
                               result2_2=result2_2,
                               collectionkey=collectionkey,
                               collection2key=collection2key,
                               leadParagraph=leadParagraph,
                               subject=subject,
                               secondaryBanner1=secondaryBanner1,
                               secondaryBanner2=secondaryBanner2,
                               secondaryBanner3=secondaryBanner3,
                               secondaryBannerSectionTitle=secondaryBannerSectionTitle,
                               keuntunganDaruma=keuntunganDaruma,
                               logoDaruma=logoDaruma,
                               footer=footer,
                               secondaryBanner1Url=secondaryBanner1Url,
                               secondaryBanner2Url=secondaryBanner2Url,
                               secondaryBanner3Url=secondaryBanner3Url,
                               mainBannerUrl=mainBannerUrl,
                               footerBanner=footerBanner,
                               result=result,
                               collectionProductKeys=collectionProductKeys,
                               collection2ProductKeys=collection2ProductKeys,
                               lists=lists,
                               footerBannerUrl=footerBannerUrl)


@app.route('/send_newsletter')
def create():

    if request.method == 'GET':

        entry_id = request.args.get('entry_id')

        # get key_path
        key_path = request.args.get('collectionkey', 'null')
        key_path = str(key_path).replace('catalog/', '')
        key_path = key_path.replace('.html', '')


        # get collection key
        collectionkey = request.args.get('collectionkey')

        # get list id
        listId = request.args.get('listId')

        result = request.args.get('result', None)

        result1 = request.args.get('result1', None)

        result2 = request.args.get('result2', None)

        # get footer banner from detail page
        footerBanner = request.args.get('footerBanner')

        # get leadParagraph
        leadParagraph = request.args.get('leadParagraph')

        # get SecondaryBannerSectionTitle
        secondaryBannerSectionTitle = request.args.get('secondaryBannerSectionTitle')

        #get mainBannerUrl
        mainBannerUrl = request.args.get('mainBannerUrl')

        # get collection product keys/code
        collectionProductKeys = request.args.get('collectionProductKeys', 'null')

        # get Asset ContentBlock
        keuntunganDaruma = request.args.get('keuntunganDaruma')


        # get secondaryBanner1 url from detail page
        secondaryBanner1 = request.args.get('secondaryBanner1')


        # get secondaryBanner2 url from detail page
        secondaryBanner2 = request.args.get('secondaryBanner2')


        # get secondaryBanner3 url from detail page
        secondaryBanner3 = request.args.get('secondaryBanner3')


        # get secondaryBanner1Url from detail page
        secondaryBanner1Url = request.args.get('secondaryBanner1Url')

        # get secondaryBanner2Url from detail page
        secondaryBanner2Url = request.args.get('secondaryBanner2Url')

        # get target
        target = request.args.get('target', None)

        # get secondaryBanner3Url from detail page
        secondaryBanner3Url = request.args.get('secondaryBanner3Url')

        # get Asset logoDaruma
        logoDaruma = getAssetContentBlockLogoDaruma()
        logoDaruma = 'http://' + str(logoDaruma)


        # get banner url from detail page
        bannerurl = request.args.get('bannerurl')

        # get contentblock footer
        footer = getContentBlockFooter()

        # get subject
        subject = request.args.get('subject')

        # get footerBannerUrl
        footerBannerUrl = request.args.get('footerBannerUrl')

        # generate html file
        create_html(bannerurl=bannerurl,
                    leadParagraph=leadParagraph,
                    subject=subject,
                    secondaryBanner1=secondaryBanner1,
                    secondaryBanner2=secondaryBanner2,
                    secondaryBanner3=secondaryBanner3,
                    secondaryBanner1Url=secondaryBanner1Url,
                    secondaryBanner2Url=secondaryBanner2Url,
                    secondaryBanner3Url=secondaryBanner3Url,
                    mainBannerUrl=mainBannerUrl,
                    footerBanner=footerBanner,
                    keuntunganDaruma=keuntunganDaruma,
                    secondaryBannerSectionTitle=secondaryBannerSectionTitle,
                    logoDaruma=logoDaruma,
                    footer=footer,
                    collectionkey=collectionkey,
                    key_path=key_path,
                    result=result,
                    collectionProductKeys=collectionProductKeys,
                    footerBannerUrl=footerBannerUrl)

        target = str(target).split(',')

        # set sender
        sender = 'hello@daruma.co.id'
        if listId == 'None':
            if DEBUG_MODE == True:
                return render_template('test.html')
            else:
                res = str()
                for t in target:
                    sendEmail(sender=sender, target=t, subject=subject)
                    res += t
                flash('Test Newsletter was sent to %s' % res)
                return redirect('preview/%s' % entry_id)


        elif listId != 'None':
            if DEBUG_MODE == True:
                # insert_schedule(list_id=listId,
                #                 subject=subject,
                #                 entry_id=entry_id,
                #                 post_date=getDateNow(),
                #                 status="delivered")
                return render_template('test.html')
            else:
                # insert_schedule(list_id=listId,
                #                 subject=subject,
                #                 entry_id=entry_id,
                #                 post_date=getDateNow(),
                #                 status="delivered")
                createAndSendCampaign(subject=subject, title=subject, list_id=listId)
                flash('Test Newsletter was sent to %s' % listId)
                return redirect('preview/%s' % entry_id)

        else:
            return 'Oops try again! %s' % listId

@app.route("/view_schedule")
def view_schedule():
    schedules = get_schedule()
    return render_template("schedule_page.html", schedules=schedules)

@app.route("/cancel_schedule", methods=['GET', 'POST'])
def cancel():
    if request.method == 'POST':
        no = request.form['no']
        cancelSchedule(no)
        return redirect(url_for("view_schedule"))

@app.route("/insert_schedule", methods=['POST', 'GET'])
def schedule():
    if request.method == 'POST':
        list = request.form['listId']
        list = str(list).replace("(", "").replace(")", "")
        list_id = str(list).split(',')[1]
        list_name = str(list).split(',')[0]
        entry_id = request.form['id']
        subject = request.form['subject']
        date = request.form['dateSchedule']
        time = request.form['timeSchedule']
        post_date = "%s - %s" % (date, time)
        insert_schedule(list_id=list_id, list_name=list_name, subject=subject, entry_id=entry_id, post_date=post_date)
        flash("%s scheduled" % subject)
        return redirect("/preview/%s" % entry_id)

@app.route("/test")
def test():
    return render_template("test.html")

if '__main__' == __name__:
    if DEBUG_MODE == True:
        app.run(debug=True)
    else:
        #port = int(os.environ.get('PORT', 3000))
        port = 3000
        app.run(host='0.0.0.0', port=port)
        # app.run(debug=True)

