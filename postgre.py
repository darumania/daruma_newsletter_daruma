import psycopg2 as pg
from db_config import config
import datetime

params = config()

def connect():

    conn = None
    try:

        print("Connecting to the postgresql database..")
        conn = pg.connect(**params)

        cur = conn.cursor()

        print("PostgreSQL Database Version: ")
        cur.execute("SELECT version()")

        db_version = cur.fetchone()
        print(db_version)

        cur.close()

    except (Exception, pg.DatabaseError) as error:
        print("Ooops: ", error)
    finally:
        if conn is not None:
            conn.close()
            print("connection closed..")

def create_table():

    commands = """
    CREATE TABLE schedule (
    no SERIAL PRIMARY KEY,
    list_id VARCHAR(100) NOT NULL,
    list_name VARCHAR(100) NOT NULL,
    subject VARCHAR(100) NOT NULL,
    entry_id VARCHAR(255) NOT NULL,
    status VARCHAR(50) NOT NULL,
    post_date VARCHAR(100) NOT NULL
    )
    """

    conn = None
    try:
        conn = pg.connect(**params)
        cur = conn.cursor()

        print("creating table..")
        cur.execute(commands)

        cur.close()

        conn.commit()
        print("Table was created..")
    except (Exception, pg.DatabaseError) as e:
        print("Oops: ", e)
    finally:
        if conn != None:
            conn.close()

def insert_schedule(list_id, list_name,  subject, entry_id, post_date, status="pending"):

    commands = """
    INSERT INTO schedule(list_id, list_name, subject, entry_id, post_date, status) VALUES(%s, %s, %s, %s, %s, %s)
    """

    conn = None
    try:
        conn = pg.connect(**params)

        cur = conn.cursor()

        cur.execute(commands, (list_id, list_name, subject, entry_id, post_date, status,))

        conn.commit()
        print("%s was inserted into schedule table" % subject)

        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("Oops: ", e)
    finally:
        if conn is not None:
            conn.close()

def get_schedule():

    conn = None
    result = list()
    commands = """
    SELECT * FROM schedule ORDER BY no ASC
    """
    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(commands)

        rows = cur.fetchall()

        for row in rows:
            no = row[0]
            list_name = row[1]
            list_id = row[2]
            subject = row[3]
            entry_id = row[4]
            status = row[5]
            post_date = row[6]
            result.append(dict(no=no,
                               list_id=list_id,
                               list_name=list_name,
                               subject=subject,
                               entry_id=entry_id,
                               status=status,
                               post_date=post_date))
        cur.close()
        return result
    except (Exception, pg.DatabaseError) as e:
        print("Oops: ", e)
    finally:
        if conn is not None:
            conn.close()

def dropTabel(tableName):

    conn = None
    commands = """
    DROP TABLE IF EXISTS {table}
    """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()

        cur.execute(commands.format(table=tableName))

        conn.commit()
        print("%s table was deleted" % tableName)
        cur.close()
    finally:
        if conn is not None:
            conn.close()

def delete_record(no):
    conn = None
    command = """
    DELETE FROM schedule WHERE no=%s 
    """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(command, (no,))
        print("row with no %s was deleted" % no)
        conn.commit()
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        if conn is not None:
            conn.close()

def getDateNow():
    d = datetime.datetime.now()
    day = "0{}".format(d.day) if len(str(d.day)) == 1 else d.day
    month = d.month
    year = d.year

    hour = d.hour
    mins = d.minute
    return "%s-%s-%s - %s:%s" % (year, month, day, hour, mins)

def cancelSchedule(no):

    conn = None
    command = """
    UPDATE schedule SET status='canceled' WHERE no=%s
    """
    try:
        conn = pg.connect(**params)

        cur = conn.cursor()

        cur.execute(command, (no,))
        print("status no %s was updated to cancel" % no)

        conn.commit()
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("Oops: ", e)
    finally:
        if conn is not None:
            conn.close()

def deliveredSchedule(no):

    conn = None
    command = """
    UPDATE schedule SET status='delivered' WHERE no=%s
    """
    try:
        conn = pg.connect(**params)

        cur = conn.cursor()

        cur.execute(command, (no,))
        print("status no %s was updated to delivered" % no)

        conn.commit()
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("Oops: ", e)
    finally:
        if conn is not None:
            conn.close()

def errorSchedule(no):

    conn = None
    command = """
    UPDATE schedule SET status='error' WHERE no=%s
    """
    try:
        conn = pg.connect(**params)

        cur = conn.cursor()

        cur.execute(command, (no,))
        print("status no %s was updated to error" % no)

        conn.commit()
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("Oops: ", e)
    finally:
        if conn is not None:
            conn.close()

def get_pending_newsletter():
    conn = None
    command = """
    SELECT * FROM schedule WHERE status='pending'
    """
    result = list()

    try:
        conn = pg.connect(**params)

        cur = conn.cursor()
        cur.execute(command)

        rows = cur.fetchall()

        for row in rows:
            no = row[0]
            list_name = row[1]
            list_id = row[2]
            subject = row[3]
            entry_id = row[4]
            status = row[5]
            post_date = row[6]
            result.append(dict(no=no,
                          list_id=list_id,
                          list_name=list_name,
                          subject=subject,
                          entry_id=entry_id,
                          status=status,
                          post_date=post_date))
        cur.close()
        return result
    except (Exception, pg.DatabaseError) as e:
        print("Oops: ", e)
    finally:
        if conn is not None:
            conn.close()

if __name__ == "__main__":
    # create_table()
    # cancelSchedule(7)
    # insert_schedule(subject="Testing schedule", entry_id="16263172312ha81", post_date="22010123")
    # print(get_pending_newsletter())
    # dropTabel("schedule")
    # delete_record(1)
    # deliveredSchedule(8)
    # print(get_pending_newsletter())
    connect()