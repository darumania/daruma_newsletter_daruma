import datetime
import dateutil.parser
from api import *
import time
import json
import markdown2
from postgre import *
from pymemcache.client import base
import threading

markdowner = markdown2.Markdown()

def autosend_createHTML(content_id):

    content = getContentById(content_id)

    bannerurl = getAsset(content_id)
    collectionkey = content.get("collectionKey", "null")
    key_path = str(collectionkey).replace("/catalog/", "")\
        .replace("catalog/", "")\
        .replace(".html","") if collectionkey != "null" else "null"
    leadParagraph = content.get('bodyText', None)
    leadParagraph = markdowner.convert(leadParagraph) if leadParagraph is not None else 'null'
    subject = content.get("subject", "null")
    secondaryBanner1 = getSecondaryBanner1(content_id)
    secondaryBanner2 = getSecondaryBanner2(content_id)
    secondaryBanner3 = getSecondaryBanner3(content_id)
    secondaryBannerSectionTitle = content.get('secondaryBannerSectionTitle', "null")
    keuntunganDaruma = "http://" + str(getAssetContentBlock())
    logoDaruma = "http://" + str(getAssetContentBlockLogoDaruma())
    footer = getContentBlockFooter()
    secondaryBanner1Url = content.get('secondaryBanner1Url', None)
    secondaryBanner2Url = content.get('secondaryBanner2Url', None)
    secondaryBanner3Url = content.get('secondaryBanner3Url', None)
    mainBannerUrl = content.get('mainBannerUrl', None)
    footerBanner = getFooterBanner(content_id)
    collectionProductKeys = content.get('collectionProductKeys', 'null')
    footerBannerUrl = content.get('footerBannerUrl', None)

    if DEBUG_MODE == True:
        result = getProductByProductKeys(intoList(collectionProductKeys)) if collectionProductKeys is not 'null' else getProducts(key_path)
    else:
        try:
            result = getProductByProductKeys(
                intoList(collectionProductKeys)) if collectionProductKeys is not 'null' else getProducts(key_path)
        except Exception:
            result = ''

    result1 = result[:3]
    result2 = result[3:]

    PATH = os.path.dirname(os.path.abspath('__file__'))
    TEMPLATE_ENVIRONMENT = Environment(autoescape=False, loader=FileSystemLoader(os.path.join(PATH, 'templates')),trim_blocks=False)

    def render_templates(template_filename, context):
        return TEMPLATE_ENVIRONMENT.get_template(template_filename).render(context)

    context = {
        'result': result,
        'bannerurl': bannerurl,
        'key_path': key_path,
        'mainBannerUrl': mainBannerUrl,
        'result1': result1,
        'result2': result2,
        'collectionkey': collectionkey,
        'leadParagraph': leadParagraph,
        'subject': subject,
        'secondaryBanner1': secondaryBanner1,
        'secondaryBanner2': secondaryBanner2,
        'secondaryBanner3': secondaryBanner3,
        'secondaryBanner1Url': secondaryBanner1Url,
        'secondaryBanner2Url': secondaryBanner2Url,
        'secondaryBanner3Url': secondaryBanner3Url,
        'secondaryBannerSectionTitle': secondaryBannerSectionTitle,
        'keuntunganDaruma': keuntunganDaruma,
        'logoDaruma': logoDaruma,
        'footer': footer,
        'footerBanner': footerBanner,
        'footerBannerUrl': footerBannerUrl
    }

    if DEBUG_MODE == True:
        with open('templates/test.html', 'w') as f:
            html = render_templates('template.html', context)
            f.write(html)
        print("html was created :D")
    else:
        with open('email.html', 'w') as f:
            html = render_templates('template.html', context)
            f.write(html)
            print("html was created :D")


def getDate(date):
    en = dateutil.parser.parse(date)
    day = en.day
    month = en.month
    year = en.year

    hour = en.hour
    mins = en.minute
    return "%s:%s:%s - %s:%s" % (day, month, year, hour, mins)

def getDateNow():
    d = datetime.datetime.now()
    day = d.day
    month = d.month
    year = d.year

    hour = d.hour
    mins = d.minute
    return "%s:%s:%s - %s:%s" % (day, month, year, hour, mins)

def getTime():
    d = datetime.datetime.now()
    hour = d.hour
    minutes = d.minute
    return "%s:%s" % (hour, minutes)

def caching():
    client = base.Client(("localhost", 11211))
    while True:
        data = get_pending_newsletter()
        client.set("data", json.dumps(data))
        print(threading.current_thread().getName(),">>>>>>>>> data cached <<<<<<<<<<")
        time.sleep(3600)

def run_flask():
    os.system("python3 app.py > /root/newsletter_flask.log 2>&1")

def main_service():
    # client = base.Client(("localhost", 11211))
    print(threading.current_thread().getName(), "is now running..")
    while True:

        # entries = json.loads(client.get("data"))
        entries = get_pending_newsletter()



        for entry in entries:
            now = getDateNow()
            d = entry.get("post_date", None)
            if d != None:
                dat = getDate(d)
                if dat == now:
                    # add send method
                    try:
                        autosend_createHTML(entry.get("entry_id"))
                        createAndSendCampaign(subject=entry.get("subject"),
                                              title=entry.get("subject"),
                                              list_id=entry.get("list_id"))
                        deliveredSchedule(int(entry.get("no")))
                        print("cooling down for 1 minute..")
                        time.sleep(60)
                    except:
                        errorSchedule(int(entry.get("no")))
                else:
                    print("%s not today, postdate: %s, now: %s, entry_id:%s" % (entry.get("no"), dat, now, entry.get("entry_id")))
            else:
                print("%s not today" % entry.get("no"))
        # print("checking done..")
        # print("Please wait..")
        for i in reversed(range(10, 1)):
            print("engine started in %ss" % i)
            time.sleep(1)

if __name__ == '__main__':
    main_service()