as postgres: 
createdb -p 5433 daruma_newsletter
psql
CREATE USER dnews WITH PASSWORD 'daruma123';
GRANT ALL PRIVILEGES ON DATABASE daruma_newsletter TO dnews;

as normal user:
psql -h localhost -p 5433 -U dnews -W daruma_newsletter

CREATE TABLE schedule (
    no SERIAL PRIMARY KEY,
    list_id VARCHAR(100) NOT NULL,
    list_name VARCHAR(100) NOT NULL,
    subject VARCHAR(100) NOT NULL,
    entry_id VARCHAR(255) NOT NULL,
    status VARCHAR(50) NOT NULL,
    post_date VARCHAR(100) NOT NULL
);

