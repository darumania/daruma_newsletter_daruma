certifi==2018.8.24
chardet==3.0.4
click==6.7
contentful==1.10.2
Flask==1.0.2
idna==2.7
itsdangerous==0.24
Jinja2==2.10
markdown2==2.3.5
MarkupSafe==1.0
psycopg2==2.7.5
psycopg2-binary==2.7.5
pymemcache==2.0.0
python-dateutil==2.7.3
python-http-client==3.1.0
requests==2.19.1
sendgrid==5.6.0
six==1.11.0
urllib3==1.23
Werkzeug==0.14.1
