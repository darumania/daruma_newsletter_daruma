from autosend import run_flask, main_service
import threading


def run():
    s = threading.Thread(name="main service", target=main_service)
    f = threading.Thread(name="flask", target=run_flask)

    s.start()
    f.start()


if __name__ == "__main__":
    run()