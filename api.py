from contentful import Client
import requests
import os
from jinja2 import Environment, FileSystemLoader
import sendgrid
from sendgrid.helpers.mail import *
import markdown2
import json
from _constants import *
import sys



# contentful API
client = Client('r5mgd95bqsb5', '78d652de927d1cfd7be497b5c67e2e2fe0353f69c5eab85878857353380d616e', environment='master')
entries = client.entries({'content_type': 'promoNewsletter', 'select': 'fields'})
contentBlock = client.entry('1VJq49D4xGwqAKEkSg4uas')

# sendgrid API client
sg = sendgrid.SendGridAPIClient('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')


markdowner = markdown2.Markdown()

# 1st get all entries from promo news letter content type
def getEntry():
    result = []
    no = 1

    for e in range(len(entries)):
        postDate = entries[e].raw['fields'].get('postDate', None)
        subject = entries[e].raw['fields'].get('subject', None)
        mdleadparagraph = entries[e].raw['fields'].get('bodyText', None)
        leadParagraph = markdowner.convert(mdleadparagraph) if mdleadparagraph != None else None
        collectionProductKeys = entries[e].raw['fields'].get('collectionProductKeys', None)
        collectionProductKeys = collectionProductKeys.split(',') if collectionProductKeys != None else None
        id = entries[e].raw['sys']['id']
        secondaryBannerSectionTitle = entries[e].raw['fields'].get("secondaryBannerSectionTitle", None)
        secondaryBanner1Url = entries[e].raw['fields'].get("secondaryBanner1Url", None)
        secondaryBanner2Url = entries[e].raw['fields'].get("secondaryBanner1Url", None)
        secondaryBanner3Url = entries[e].raw['fields'].get("secondaryBanner1Url", None)
        x = dict(no=no,
                 postDate=postDate,
                 subject=subject,
                 leadParagraph=leadParagraph,
                 id=id,
                 secondaryBannerSectionTitle=secondaryBannerSectionTitle,
                 secondaryBanner1Url=secondaryBanner1Url,
                 secondaryBanner2Url=secondaryBanner2Url,
                 secondaryBanner3Url=secondaryBanner3Url,
                 collectionProductKeys=collectionProductKeys)
        result.append(x)
        no += 1
    return result

# then use getEntry() to get content based id
def getContent(index):
    entries = getEntry()
    id = entries[int(index)].get("id")
    result = client.entry(id)
    return result.raw.get("fields")

def getContentById(id):
    result = client.entry(id)
    return result.raw.get("fields")

# get asset
# def getAsset(index):
#     id = getContent(index)['mainBanner']['sys'].get('id', None)
#     asset = client.asset(id)
#     url = asset.url()
#     return url[2:]

# get asset
def getAsset(index):
    id = getContentById(index).get('mainBanner', None)
    id = id['sys']['id'] if id is not None else None
    if id is not None:
        asset = client.asset(id)
        url = asset.url()
        return url[2:]
    return 'null'

# get asset contentBlock
def getAssetContentBlock():
    id = contentBlock.raw['fields']['image']['sys']['id']
    asset = client.asset(id)
    url = asset.url()
    return url[2:]

# get asset contentBlock Logo Daruma
def getAssetContentBlockLogoDaruma():
    logoDaruma = client.entry('6aBnIO9oUEqm4mygkkUIOq')
    id = logoDaruma.raw['fields']['image']['sys']['id']
    asset = client.asset(id)
    url = asset.url()
    return url[2:]

# get contentBlock Footer
def getContentBlockFooter():
    footer = client.entry('6aBnIO9oUEqm4mygkkUIOq')
    content = footer.raw['fields']['content']
    content = markdowner.convert(content)
    return content

# get secondaryBanner1
# def getSecondaryBanner1(index):
#     id = getContent(index)['secondaryBanner1']['sys'].get('id', None)
#     asset = client.asset(id)
#     url = asset.url()
#     return url[2:]

def getSecondaryBanner1(index):
    id = getContentById(index).get('secondaryBanner1', None)
    id = id['sys']['id'] if id is not None else None
    if id is not None:
        asset = client.asset(id)
        url = asset.url()
        return url[2:]
    return 'null'


# get secondaryBanner2
# def getSecondaryBanner2(index):
#     id = getContent(index)['secondaryBanner2']['sys'].get('id', None)
#     asset = client.asset(id)
#     url = asset.url()
#     return url[2:]

def getSecondaryBanner2(index):
    id = getContentById(index).get('secondaryBanner2', None)
    id = id['sys']['id'] if id is not None else None
    if id is not None:
        asset = client.asset(id)
        url = asset.url()
        return url[2:]
    return 'null'

# get Collection product keys
def getCollectionProductKeys(index):
    index = int(index)
    try:
        content = getContent(index)['collectionProductKeys']
        content = content.split(',') if content != None else None
        return list(content)
    except Exception:
        return None

def intoList(collectionProductKeys=str()):
    result = collectionProductKeys.replace(' ', '').split(',')
    return list(result)

# get products by list of product keys
def getProductByProductKeys(prodcutKeys):
    result = list()
    for k in prodcutKeys:
        urlproduct_code = 'https://api.daruma.co.id/v1/store/product/%s/' % k
        response = requests.get(url=urlproduct_code)
        name = response.json().get('name', None)
        key = response.json().get('key', None)
        product_url = "https://daruma.co.id/product/%s%s" % (key, '.html')
        price = response.json().get('price', None)
        price = int(price)*1.1 if price is not None else None
        price = int(price) if price is not None else None
        price = '{:,}'.format(price) if price is not None else None
        price_before_discount = response.json().get('price_before_discount', None)
        price_before_discount = int(price_before_discount)*1.1 if price_before_discount is not None else None
        price_before_discount = int(price_before_discount) if price_before_discount is not None else None
        price_before_discount = '{:,}'.format(price_before_discount) if price_before_discount is not None else None
        img_url = response.json().get('primary_image_url', None)
        catalog_code = k
        wrap = dict(name=name, price=price, img_url=img_url, catalog_code=catalog_code,
                    product_url=product_url, price_before_discount=price_before_discount if price != price_before_discount else '')
        result.append(wrap)
    return result


# get secondaryBanner3
# def getSecondaryBanner3(index):
#     id = getContent(index)['secondaryBanner3']['sys'].get('id', None)
#     asset = client.asset(id)
#     url = asset.url()
#     return url[2:]

def getSecondaryBanner3(index):
    id = getContentById(index).get('secondaryBanner3', None)
    id = id['sys']['id'] if id is not None else None
    if id is not None:
        asset = client.asset(id)
        url = asset.url()
        return url[2:]
    return 'null'


# get footer banner
# def getFooterBanner(index):
#     id = getContent(index).get('footerBanner', None)
#     if id != None:
#         id = id['sys']['id']
#         asset = client.asset(id)
#         url = asset.url()
#         return url[2:]
#     return None

# get footer banner
def getFooterBanner(index):
    id = getContentById(index).get('footerBanner', None)
    if id != None:
        id = id['sys']['id']
        asset = client.asset(id)
        url = asset.url()
        return url[2:]
    return 'null'

# get products from daruma API search
def getProducts(key_path):
    daruma_api_search = 'https://api.daruma.co.id/v1/store/search/'
    parameter = {
        'offset': 6,
        'size': 6,
        'key_path': key_path,
        'brand_size': 6
    }

    response = requests.get(daruma_api_search, params=parameter)
    response = response.json().get("products")

    result = list()
    for res in range(len(response)):
        product_img_url = response[res].get('images')[0]['original_url']
        # product_name = response[res].get('name')[:20] + '..'
        product_name = response[res].get('name')
        product_catalog_code = response[res].get('catalog_code')
        product_price = response[res].get('price')
        product_price = int(product_price) * 1.1
        product_price = int(product_price)
        product_price = '{:,}'.format(product_price)
        price_before_discount = response[res].get('price_before_discount', None)
        price_before_discount = int(price_before_discount) * 1.1 if price_before_discount is not None else None
        price_before_discount = int(price_before_discount)
        price_before_discount = '{:,}'.format(price_before_discount) if price_before_discount is not None else None
        product_url = "https://daruma.co.id/product/%s%s" % (response[res].get('key'), '.html')
        product_availibility = "Tersedia" if response[res].get('has_availability') else "Indent"
        if product_price != price_before_discount:
            d = dict(name=product_name, img_url=product_img_url, catalog_code=product_catalog_code,
                     price=product_price, availibility=product_availibility, product_url=product_url,
                     price_before_discount=price_before_discount)
            result.append(d)
        elif product_price == price_before_discount:
            d = dict(name=product_name, img_url=product_img_url, catalog_code=product_catalog_code,
                     price=product_price, availibility=product_availibility, product_url=product_url,
                     price_before_discount='')
            result.append(d)

    return result

# html creator
def create_html(bannerurl, key_path, collectionkey, leadParagraph, subject, secondaryBanner1,
                secondaryBanner2, secondaryBanner3, secondaryBannerSectionTitle, keuntunganDaruma,
                logoDaruma, footer, secondaryBanner1Url, secondaryBanner2Url, secondaryBanner3Url,
                mainBannerUrl, footerBanner, result, collectionProductKeys, footerBannerUrl):


    PATH = os.path.dirname(os.path.abspath(__file__))
    TEMPLATE_ENVIRONMENT = Environment(autoescape=False, loader=FileSystemLoader(os.path.join(PATH, 'templates')),
                                       trim_blocks=False)

    def render_templates(template_filename, context):
        return TEMPLATE_ENVIRONMENT.get_template(template_filename).render(context)

    # if DEBUG_MODE:
    #     result = getProductByProductKeys(intoList(collectionProductKeys)) if collectionProductKeys != 'None' else getProducts(key_path)
    # else:
    #     try:
    #         result = getProductByProductKeys(
    #             intoList(collectionProductKeys)) if collectionProductKeys != 'None' else getProducts(key_path)
    #     except Exception:
    #         result = ''

    result = getProductByProductKeys(intoList(collectionProductKeys)) if collectionProductKeys != 'null' else getProducts(key_path)
    # result_2 = getProductByProductKeys(
    #     intoList(collection2ProductKeys)) if collection2ProductKeys != 'null' else getProducts(key_path2)

    # get collection key


    result1 = result[:3]
    result2 = result[3:6]

    # this query gona used in template
    context = {
        'result': result,
        'bannerurl': bannerurl,
        'key_path' : key_path,
        'mainBannerUrl' : mainBannerUrl,
        'result1' : result1,
        'result2' : result2,
        'collectionkey' : collectionkey,
        'leadParagraph' : leadParagraph,
        'subject' : subject,
        'secondaryBanner1' : secondaryBanner1,
        'secondaryBanner2' : secondaryBanner2,
        'secondaryBanner3' : secondaryBanner3,
        'secondaryBanner1Url' : secondaryBanner1Url,
        'secondaryBanner2Url' : secondaryBanner2Url,
        'secondaryBanner3Url' : secondaryBanner3Url,
        'secondaryBannerSectionTitle' : secondaryBannerSectionTitle,
        'keuntunganDaruma' : keuntunganDaruma,
        'logoDaruma' : logoDaruma,
        'footer' : footer,
        'footerBanner' : footerBanner,
        'footerBannerUrl' : footerBannerUrl
    }

    # production
    if DEBUG_MODE == True:
        with open('templates/test.html', 'w', encoding=sys.getdefaultencoding()) as f:
            html = render_templates('template.html', context)
            f.write(html)
        print("html was created :D")
    else:
        with open('email.html', 'w', encoding=sys.getdefaultencoding()) as f:
            html = render_templates('template.html', context)
            f.write(html)
        print("html was created :D")


# send email function
def sendEmail(sender, target, subject):


    from_email = Email(sender)
    to_email = Email(target)
    subject = subject

    html = open('email.html', encoding=sys.getdefaultencoding()).read()

    content = Content("text/html", html)
    mail = Mail(from_email, subject, to_email, content)

    response = sg.client.mail.send.post(request_body=mail.get())

    print(response.status_code)
    # print(response.body)
    # print(response.headers)

def createCampaign(subject, list_id):

    sender_id = 315045

    html = open('email.html', encoding=sys.getdefaultencoding()).read()

    data = {
        "categories": [
            ""
        ],
        "custom_unsubscribe_url": "",
        "html_content": html,
        "ip_pool": "",
        "list_ids": [
            list_id
        ],
        "plain_content": subject,
        "segment_ids": [
            ""
        ],
        "sender_id": sender_id,
        "subject": subject,
        "suppression_group_id": 19893,
        "title": subject
    }

    response = sg.client.campaigns.post(request_body=data)
    print(response.body)


# retrieve all list
def getSendgridLists():
    sendgrid_api_key = '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'
    headers = {
        'Authorization': 'Bearer %s' % sendgrid_api_key,
        'Content-Type': 'application/json'
    }



    url = 'https://api.sendgrid.com/v3/contactdb/lists'
    response = requests.get(url=url, headers=headers)
    response = response.json()['lists']
    lists = []

    for i in response:
        d = dict(name=i['name'], id=i['id'])
        lists.append(d)
    return lists

# get list ID
def getListId(name):
    lists = getSendgridLists()
    if name == '' or name == None:
        return None
    for x in lists:
        if name in x['name']:
            return x.get('id', None)


def createAndSendCampaign(subject, title, list_id):
    sendgrid_api_key = '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'
    sender_id = 317645

    if DEBUG_MODE == True:
        html = open('templates/test.html', encoding=sys.getdefaultencoding()).read()
    else:
        html = open('email.html', encoding=sys.getdefaultencoding()).read()

    data = {
        "categories": [
            "daruma",
        ],
        "custom_unsubscribe_url": "",
        "html_content": str(html),
        "ip_pool": "",
        "list_ids": [
            list_id,
        ],
        "plain_content": "<p>click here <%asm_group_unsubscribe_url%> if you would like to unsubscribe and stop receiving these emails</p>",
        "segment_ids": [],
        "sender_id": sender_id,
        "subject": subject,
        "suppression_group_id": 19893,
        "title": title
    }

    url = 'https://api.sendgrid.com/v3/campaigns'

    headers = {
        'Authorization' : 'Bearer %s' % sendgrid_api_key,
        'Content-Type' : 'application/json'
    }

    r = requests.post(url=url, data=json.dumps(data, indent=4), headers=headers)
    campaign_id = r.json()['id']

    url = 'https://api.sendgrid.com/v3/campaigns/%s/schedules/now' % campaign_id
    headers = {
        'Authorization': 'Bearer %s' % sendgrid_api_key,
        'Content-Type': 'application/json'
    }
    response = requests.post(url=url, headers=headers)
    print("response status: ")
    print(response.status_code)
    print(response.text)
    print("email was sent..")

def getLists():
    sendgrid_api_key = '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'
    headers = {
        'Authorization': 'Bearer %s' % sendgrid_api_key,
        'Content-Type': 'application/json'
    }

    url = 'https://api.sendgrid.com/v3/contactdb/lists'
    response = requests.get(url=url, headers=headers)
    response = response.json()['lists']
    lists = []
    for i in response:
        try:
            params = {
                'page': 1,
                'page_size': 1,
                'list_id': i['id']
            }
            response_count = sg.client.contactdb.lists._(i['id']).recipients.get(query_params=params)
            response_count = json.loads(response_count.body.decode())['recipient_count']
            d = dict(name=i['name'], id=i['id'], count=response_count)
            lists.append(d)
        except:
            print(f'error getting lists of users {i}')
            continue
    return lists

# if __name__ == "__main__":
    # print(json.dumps(getContentById(id='3Paae45R9YIIESIAM04EcI'), indent=4))
    # print(getContentById(id='3Paae45R9YIIESIAM04EcI').get('collection2ProductKeys', None))
    # print(json.dumps(client.entry('3Paae45R9YIIESIAM04EcI').raw, indent=4))