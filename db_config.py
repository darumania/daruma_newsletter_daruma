from configparser import ConfigParser

def config():
    filename = "database.ini"
    section = "postgresql"

    #create parser
    parser = ConfigParser()

    # read database.ini
    parser.read(filename)

    db = {}
    if parser.has_section(section):
        params = parser.items(section)
        for param in params:
            db[param[0]] = param[1]
    else:
        raise Exception("Section %s not found in the %s file" % (section, filename))

    return db